package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kazakov.iteco.api.repository.IRoleRepository;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.model.Role;

@Service
@Transactional
public class RoleService implements IRoleService {

    @NotNull
    @Autowired
    private IRoleRepository repository;

    public void save(Role role) {
        repository.save(role);
    }

}
