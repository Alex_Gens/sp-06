package ru.kazakov.iteco.validator;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.model.User;

@Component
public class UserValidator implements Validator {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private PasswordEncoder encoder;


    @Override
    public boolean supports(@NotNull final Class<?> aClass) {return User.class.equals(aClass);}

    @SneakyThrows
    @Override
    public void validate(@Nullable final Object o, @NotNull final Errors errors) {
        @Nullable final User user = (User) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "user.required.field");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "user.required.field");
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "user.already.exist");
        }
    }

}
